from django.db import models
from django.utils.translation import gettext_lazy as _
from apps.patient.models import Patient
from apps.procedure.models import Procedure
from django.utils import formats


class ScheduleTypes(models.Model):
    name = models.CharField(
        verbose_name=_("Name"), max_length=80,
        unique=True, db_index=True
    )

    class Meta:
        verbose_name = _("Schedule Type")
        verbose_name_plural = _("Schedule Types")
        ordering = ['name']

    def __str__(self):
        return self.name


class Schedule(models.Model):
    type = models.ForeignKey(
        verbose_name=_("Schedule Type"), to=ScheduleTypes,
        null=True, on_delete=models.SET_NULL
    )
    patient = models.ForeignKey(
        verbose_name=_("Patient"), to=Patient,
        related_name='schedule_patients', on_delete=models.CASCADE
    )
    procedure = models.ForeignKey(
        verbose_name=_("Procedure"), to=Procedure,
        related_name='schedule_procedures', on_delete=models.CASCADE
    )
    date = models.DateField(
        verbose_name=_("Date"), db_index=True
    )
    start_time = models.TimeField(
        verbose_name=_("Start Time"), db_index=True
    )
    end_time = models.TimeField(
        verbose_name=_("End Time"), db_index=True
    )

    class Meta:
        verbose_name = _("Schedule")
        verbose_name_plural = _("Schedules")
        ordering = ['date', 'start_time', 'end_time']

    def __str__(self):
        return "{schedule}: {date} {start_time}-{end_time}".format(
            schedule=_("Schedule"),
            date=formats.date_format(self.date, 'SHORT_DATE_FORMAT', True),
            start_time=formats.date_format(self.start_time, 'TIME_FORMAT', True),
            end_time=formats.date_format(self.end_time, 'TIME_FORMAT', True)
        )
