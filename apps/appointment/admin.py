from django.contrib import admin
from .models import ScheduleTypes, Schedule


@admin.register(ScheduleTypes)
class ScheduleTypeAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    autocomplete_fields = ['patient', 'type', 'procedure']
    list_display = ['patient', 'type', 'procedure', 'date', 'start_time', 'end_time']
    list_filter = ['type', 'procedure', 'date']
    search_fields = ['patient']