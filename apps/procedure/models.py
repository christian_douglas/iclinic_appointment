from django.db import models
from django.utils.translation import gettext_lazy as _


class Procedure(models.Model):
    name = models.CharField(
        verbose_name=_("Name"), max_length=80, unique=True,
        db_index=True
    )

    class Meta:
        verbose_name = _("Procedure")
        verbose_name_plural = _("Procedures")
        ordering = ['name']

    def __str__(self):
        return self.name
