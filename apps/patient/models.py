from django.db import models
from django.utils.translation import gettext_lazy as _


GENDE_MALE = 'M'
GENDER_FEMALE = 'F'
GENDER_OTHER = 'O'

GENDER_CHOICES = [
    (GENDE_MALE, _("Male")),
    (GENDER_FEMALE, _("Fenale")),
    (GENDER_OTHER, _("Other")),
]


class Patient(models.Model):
    name = models.CharField(
        verbose_name=_("Name"), max_length=160,
        db_index=True
    )
    date_of_birth = models.DateField(
        verbose_name=_("Date of birth")
    )
    gender = models.CharField(
        verbose_name=_("Gender"), max_length=1,
        choices=GENDER_CHOICES
    )
    email = models.EmailField(
        verbose_name=_("E-Mail"), blank=True, null=True, unique=True
    )

    class Meta:
        verbose_name = _("Patient")
        verbose_name_plural = _("Patients")

        ordering = ['name']

    def __str__(self):
        return self.name